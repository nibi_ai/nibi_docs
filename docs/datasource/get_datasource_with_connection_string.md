# Overview

Download of configuration file for Data Source with connection string.  

## Get Datasource
----------------------------------------------------------
URL : `/v1/admin/nibiconf/data_source/<data_source>`

Method : `GET`


#### Parameters
Name  | Type | Description
------------- | -------------- | -------------
`data_source` | `string` | A datasource name that will be passed as path variable

#### Output : JSON 
The file in a json format 
```json
{
  "id": "13837135",
  "SqlName": "SqlName",
  "columns": [
    {
      "name": "label" ,
      "type": "text",
      "SqlName": "sql_name_label",
      "distinctValues": [],
      "Entity": "location"
    }
  ],
  "timezone": "UTC-12:00",
  "jaqlSupport": "yes",
  "executeSql": "yes",
  "connectionString":"pyodbc://user@sql_database:password@sql_server.database.windows",
  "name": "name"
}
```