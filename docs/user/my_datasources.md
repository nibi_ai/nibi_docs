## api/admin/data_source/<ds_name>
------------------------------------------------------------
Get List of the ds users and roles of the ds_name

Login required   | Allowed Users   | Methods 
-------------| -------------- | -------------
Yes | dataSourceOwner for data source Or Admin has acces for all data sources |  POST ,GET


!!! note "Roles"
   dataSourceEdiitor, DatasourceViewer, dataSourceOwner

### Response
```json
{
  "out": [
	{
  	"role": "datasourceowner",
  	"user_mail": "member@example.com"
	},
	{
  	"role": "dataSourceEditor",
  	"user_mail": "admin@nibiexample.com"
	}
  ]
}
```