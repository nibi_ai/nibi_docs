## `/me`
------------------------------------------------------------

Login Required  | Allowed Users  | Methods 
-------------| -------------- | -------------
Yes |  All  | GET POST

Return My details and list of datasources I have permissions to + my permissions.

### Response

```json
{
  "out": [
	{
  	"role": "datasourceowner",
  	"datasource": "member@example.com"
	},
	{
  	"role": "dataSourceEditor",
  	"datasource": "admin@nibiexample.com"
	}
  ]

```
