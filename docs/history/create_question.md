## POST
This endpoint will allow you to insert question  to the loger get questions and query questions.

URL : `/api/history/v1/questions`

Method : `POST`

Login required   | Allowed Users     | Methods   
-------------| -------------- | -------------
Yes | All  | POST

### Parameters : JSON
Time   | The time the question was asked  | Not a parameter a filed in the table   
-------------| -------------- | -------------
`org_id` | The Org ID of the user  | Mandatory
`user_id` | The user ID  | Mandatory
`data_source` | The Data source the question was asked about | Mandatory
`question` | The question in natural language | Mandatory
`SQL` | Result SQL | Not Mandatory
`JAQL` | REsult JAQL | Not Mandatory
`data` | Result DAta if allowed Should be empty | Not Mandatory
`error`  | If Any Error | Mandatory
`Internal representation` | An internal representation please allow 10K | Not Mandatory

### Response

!!! success

```json 
{
  "action":True,
   "msg":"OK",
   "ID":the question id 
   } 
```

The question is is a unique identifier for the question so we can later set it as bookmark and 
restore it by ID or shae by id

!!! error

```json
 {
   “action”:False, 
   “msg”:”ERROR in user terms"
   }
```
