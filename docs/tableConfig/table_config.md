# Overview

## Table Configuration json
----------------------------------------------------------
Configuration of a table has two files 

SQL files and JSON file 

The SQL file will include a create table will the column names 


Name  | Type | Description
------------- | -------------- | -------------
`Id` | `str` | 
`name` | `str` | The table name by the sql.
`Columns` | `List of objects` | See below for objects features.
Lior to add |  | 


## Column Object configuration
----------------------------------------------------------

Name  | Type | Description
-------------| -------------- | -------------
`name` | `str` | The name of the files in natural languish.
`type` | `str` | The table name curently supporting :    ●Text    ●	boolean  ●	Date  [Just one ]   ● Number
`distinctvalues` | `List` | See below for objects features.
`Entity` | `str` | The entity of the column.
`defaultOnSelect` | boolean | If the column will be on by default in the select clause.
`SqlName` | `str` | The sql name of the column.

Also a json file 
```json
{
   "Id":"1-1000181-1",
   "name":"companies_info",
   "Columns":[
      {
         "name":"company",
         "type":"text",
         "distinctValues":[

         ],
         "Entity":"organization",
         "defaultOnSelect":true
      },
      {
         "name":"Sector_name",
         "type":"text",
         "distinctvalues":[
            "Advertising",
            "AgTech",
            "CleanTech",
            "Cyber Security",
            "EdTech",
            "E-ommerce",
            "FinTech",
            "FoodTech",
            "Gaming",
            "Healthcare",
            "Industry-4.0",
            "IOT",
            "Other",
            "Pharmaceuticals",
            "Telecommunication",
            "Transportation"
         ],
         "Entity":"none"
      },
      {
         "name":"label",
         "type":"text",
         "SqlName":"sql_name_label",
         "distinctValues":[

         ],
         "Entity":"location"
      },
      {
         "name":"Text/background colour",
         "type":"text",
         "distinctValues":[
            "Red",
            "pink",
            "blue"
         ],
         "Entity":"colour"
      }
   ]
}
```