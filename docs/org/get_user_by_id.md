## GET(user)

URL :  `/api/admin/v1/org_users/<user_id>`

Method : 'GET'

Login required   |   Allowed Users  | Method
------------ | ------------- | -------------
Yes | OrgAdmin | POST

This endpoint will let you get user details and to delete users 
Use token to access this method 

### Parameters: Query String

Name  | Describe  | Note
------------ | ------------- | -------------
userId | user id  | Mandatory

### Response : JSON
 Json describing the user 
```json
 {
  'id': 4, 
  'active': True,
  'email': 'test_org_user2', 
  'email_confirmed_at': '2018-10-24 08:09:02.271751', 
  'first_name': '', 
  'last_name': '', 
  'country': None, 
  'phone': None, 
  'data_sources': [],
  'orgs': [{'id': 1, 'name': 'test_org'}], 
  'roles': []
}
```