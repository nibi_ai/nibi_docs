
#### Sample format of DATASOURCE_SCHEMA

```json
{
   "id":"54971965",
   "SqlName":"roydemo",
   "columns":[
      {
         "SqlName":"ts",
         "Entity":"None",
         "defaultOnSelect":false,
         "distinctValues":[
            ""
         ],
         "name":"ts",
         "type":"date",
         "nullable":"",
         "id":"57711215",
         "defaultDate":false
      }
   ],
   "timezone":"",
   "jaqlSupport":"no",
   "executeSql":"yes",
   "name":"roydemo"
}
```

!!! note "name, columns, jaqlSupport and executeSql are required fields"