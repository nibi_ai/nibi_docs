
## /data_permission/<ds_name>/remove_user
--------------------------------------------------------
Remove user role form data source

Login required  | Allowed Users   | Methods 
-------------| -------------- | -------------
Yes | dataSourceOwner for data source Or Admin has acces for all data sources | POST ,GET

### Parameters 
Name  | Describe   | Note 
-------------| -------------- | -------------
email | The User email  | Mandatory
role | The user user role   | Not mandatory

## Request: JSON
```json
{
"email":"admin@nibiexample.com",
"role":"dataSourceEditor"
}
```

### Response : JSON
```json
{
  "action":True,
   "msg":"OK"
}
```
