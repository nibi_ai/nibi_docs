# Nibi Search overview

**Version: 1.0**

**Last updated: 20 Jan 2019**


Nibi search is a liabrary that can be used in two different ways

#### Embed a Widget

Nibi widget is a embeddable component that works independently in your webpage. Nibi widget is a ReactJs component that works extremely fast in comparison to  other(AngularJs, emberjs) because of ReactJs virtual DOM and Javascript driven templates.

#### React component
React component is a npm module that is the wrapper of NIBI Search liabrary. Please read the documentation.


## Configuration properties 

Property  | Optional/Required | Description
------------ | ------------- | -------------
`API_KEY` | Required |  This is required property. You can get this property by login in your account and Developer Section(we will have this section in admin account. User can see this in his dashboard)
`DATASOURCE_NAME` | Required | This is required property. You can get source name by login in your account and sources list.
`DATASOURCE_SCHEMA` | Required | Default - 5 Number of records to be fetched
`API_BASE_URL` | Required | 

!!! warning "DATASOURCE_NAME or DATASOURCE_SCHEMA is required"

#### React component is using diffent name for configuration properties. Find the below for same

Property  | Optional/Required | Description
------------ | ------------- | -------------
`apiKey` | Required |  This is required property. You can get this property by login in your account and Developer Section(we will have this section in admin account. User can see this in his dashboard)
`datasource` | Required | This is required property. You can get source name by login in your account and sources list.
`sourceSchema` | Required | Default - 5 Number of records to be fetched
`API_BASE_URL` | Required | Default - https://ask.nibi.ai/api

!!! warning "datasource or sourceSchema is required"


## Options (Settings)

Property  | Optional/Required | Description
------------ | ------------- | -------------
`selector` | Required `(Available in widget)` | An id of HTML element where widget is rendered
`placement` | Optional **[TOP/BOTTOM]** | Default `TOP` A position of autocomplete to display from top or bottom of input field
`countOfResults` | Optional | Default - 5 Number of records to be fetched
`addBookmark` | Optional | Default - true Enables `Add bookmark` button
`showBookmark` | Optional | Default - true Enables `show bookmark` button
`showHistory` | Optional | Default - true Enables `show history` button
`autoCompleteWord` | Optional | Default - true Enables keywords/words suggestions that includes (schema fields name, and keywords like how many , that are, of etc)
`showExplainJson` | Optional | Default - false Enables `show ExplainJson` button
`autoCompleteSentense` | Optional | Default - true Enables autocomplete suggestions based on your `query text`
`containerHeight` | Optional | Default - 400px Apply max-height to autocomplete container. If height of suggestions increased more than given height `scrolling` on container is enabled.
`events` | Optional  `(Available in widget)` | Events that are triggered on specific actions by widget and callbacks are trigger with data. Please see events doc below

## Events (callbacks)
Event  | Optional/Required | Description
------------ | ------------- | -------------
`onInit` | `Optional` - onInit(datasource:any) | Triggers when widget is mounted. `datasource`  - an Json Object of datasource which is in widget.
`OnSearch` | `Optional` - onSearch(result: any) | Triggers when question is searched. result - A response of NPL that contains results, SQL query
`beforeAskQuestion` | `Optional` - beforeAskQuestion(question: string) | Triggers before a request is made to Server to get the result of question. question - returns a question string

## Widget Instance methods
Event  | Description
------------ | -------------
destroy | destroy the widget instance