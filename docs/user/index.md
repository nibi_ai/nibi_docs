#overview
Methods are web base and except parameter ad POST and GET forms 


### Summary of allowed actions by user role


&nbsp; | Read | Write | Edit | Delete |  Share  
-------------  |  --------------  |  -------------  |  -------------  |  -------------  |  -------------
Owner | Yes | Yes | Yes | Yes | Yes
Editor | Yes | Yes | Yes | No | As Editor and Viewer
Viewer | Yes | No | No | No | No

