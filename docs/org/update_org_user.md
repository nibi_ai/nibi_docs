## PUT(user)
URL :  `/api/admin/v1/org_users`

Method : 'GET'

### Parameters

Name  | Describe  | Note
------------ | ------------- | -------------
name | The name of the user to create  | Mandatory
password | The password of the created user | Mandatory 

### Response : JSON
Return JSON with user details
```json
 {
   'user_id': 3, 
   'user_email': 'test_org_user',
   'User API token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NDAzMDc2NDcsIm5iZiI6MTU0MDMwNzY0NywianRpIjoiN2Q1MTc0ZWEtODcxMS00NWUxLTliOGYtZjQxNmJiZTllMGQyIiwiZXhwIjoxNTcxODQzNjQ3LCJpZGVudGl0eSI6eyJpZCI6MSwib3JncyI6WzFdLCJhcGlfdG9rZW4iOiJiNmFhZTBjZS1kZTljLTQzMjktODczNS0xYTk4ZWQxNzAzMmIifSwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.T94JZ2Ayr73nvJGx2-DzLvONzOcBLeGAoEgp0-IoCDY'
}
```