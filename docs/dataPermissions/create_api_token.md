
## /api/admin/v1/api_tokens
------------------------------------------------------
Method : `POST`

### Parameters 
None

### Response:JSON
```json
Return api Json with the token  
{
'access_token':'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Mzc5NzQ0OTgsIm5iZiI6MTUzNzk3NDQ5OCwianRpIjoiYmQxMmMwOWYtODg5ZS00OWRjLTg0NzMtMjQ0NWIzM2FmYzRkIiwiZXhwIjoxNTY5NTEwNDk4LCJpZGVudGl0eSI6eyJpZCI6MSwiYXBpX3Rva2VuIjoiYTM5ZTg2NDAtMjYwOC00MDA0LTk4NzktOGI3ZGMwNjM0NDVkIn0sImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.Iz0TtSIcyVrN9oo3hcs2RW6BvOGodEgQBa5YmB2EbiM', 
'expiration': '31536000.0', 
'uuid': 'a39e8640-2608-4004-9879-8b7dc063445d'
}
```

