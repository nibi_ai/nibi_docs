# Overview
This Endpoint allow you to create Data source access token for a nibi virtual user.
The user Id is provided by you and this access token allow read access to a data source for up to 12 hours

## POST
---------------------------------------------------------------
URL : `/api/admin/v1/get_datasource_token`

Method : `POST`

Login required   | Allowed Users     | Methods   
-------------| -------------- | -------------
Yes | All - operations for users tokens only  | POST

Return a read access token for the datasource 
Questions for the data source  action will be registered against  the creating user Org ID user Id and UUID )


### Parameters : JSON
Name  | Describe  | Note
------------ | ------------- | -------------
data_source | The name of the Data source to allow access to   | Mandatory, You need permission to access this data source 
expiration | How long will the token be valid in seconds | Default to 1 Hour 
app_uuid | The user that the token is given to ID in your app | 
filter | Query filter you want to add  | *see notes

!!! note "Notes"
**Filter** - this is used for adding a filter to the where clause that Nibi is generating. You enter the exact format it will be used in the SQL. The filter will be in relation AND to the conditions in the natural language sentence .

**Example** - For instance if you ask the question `How many companies are B2B`
With the filter ’USER_id = 123’ you will get the following query: 
```sql
'SELECT COUNT(*) FROM features_company WHERE (LOWER(features_company.business_model) = 'b2b') AND (USER_id = 123) ORDER BY 1 DESC;'
```

### Request : JSON
```python
client.post(
  '/api/admin/v1/api_token/get_datasource_token',
  json={'data_source':'Data_3_E',
        'expiration':20,
        'app_uuid':"123",
        },
  headers={'authorization': 'Bearer ' + user_access_token}
)
```
where **user_access_token** is user access token.


### Response : JSON
Keep in mind your parameters will be in the token you need to open it in order to  see them. 
The parameters are not encrypted 

!!! success "Status code 200"

```json
{
  'access_token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NDA4MDIwMjMsIm5iZiI6MTU0MDgwMjAyMywianRpIjoiZmYwNzJlZTktMjVlZi00OGI0LWI4NzQtOWNlZGU0OGYyMWE3IiwiZXhwIjoxNTQwODAyMDQzLCJpZGVudGl0eSI6eyJpZCI6MSwib3JncyI6WzFdLCJzb3VyY2Vfcm9sZSI6eyJkcyI6IkRhdGFfM19FIiwicm9sZSI6ImRhdGFTb3VyY2VWaWV3ZXIifSwiYXBwX3V1aWQiOiIxMjMifSwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.dYm994kqCZBhWvu-2YSycFfNusM7fNutM9pg_8rX_Ps',
 'expiration': 20.0
 }

```

## Example

## Get datasource Access token Server Side

To get the datasource access token you must have an valid Read [API Key](../user/api_key_example.md) . Due to security reasons, we ask developer to get datasource access token on server side. Once you get access token that is valid for next 24hrs. Refer to the screen in the admin. 
Server side Example of

### Parameters 

Please read [Datasource parameters](../accessToken/datasource_access_token.md)


We have written an example in PHP,  You can use any server side language [python|node|java etc]

#### Example

```php
<?php 
class NibiAuth {
protected $API_KEY = 'YOUR API TOKEN; // API Key that you generate from Admin.

protected $API_URL = 'https://ask.nibi.ai/api/admin/v1/api_token/get_datasource_token';
  public function getAccessToken() {
       $request = array(
            'data_source'=> 'DATASOURCE_NAME', 
            'expiration'=>2000, 
            'app_uuid'=>77
        );
       $request_string = json_encode($request);  
       $ch = curl_init($this->API_URL);
       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
       curl_setopt($ch, CURLOPT_POSTFIELDS, $request_string);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER,
           array(
              'Authorization: Bearer ' . $this->API_KEY,
              'Content-Length: ' . strlen($request_string)
           ));
       curl_setopt($ch, CURLOPT_TIMEOUT, 5);
       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
       $response = curl_exec($ch);
       curl_close($ch);
       return $this->responseHandler($response);
   }

   private function responseHandler($response)
   {
       $result = json_decode($response, true);
       if (!is_object($result) && !is_array($result)) {
           return '';
       }
       return isset($result['access_token']) ? $result['access_token'] : '';
   }


}
$auth =  new NibiAuth();
$token = $auth->getAccessToken();
?>
```

