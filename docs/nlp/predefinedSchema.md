This method is the same as the api/ask/v2//nibitalkres APi but instead of using a predefined data source you use an ad hoc data source. You need to send the data source configuration in the request. 


URL : `/api/ask/v2/schema_nibitalkres`

Method : `POST`


#### Parameters
Name  | Type | Description
------------- | -------------- | -------------
`Question` | `string` | The question you  want translated to SQL.
`maxResults` | `string` /int | The max number of results you want to get.
`schema` | `string` | Schema describing your data source  (see *note below )
`filter` |  | Fitler - this is used for adding a filter to the where clause that Niibi is generating. You enter the exact format it will be used in the SQL. The filter will be in relation AND to the conditions in the natural language sentence . 

#### How to use filter
For instance if you ask the question `How many companies are B2B` With the filter `USER_id = 123` you will get the following query:

```sql
SELECT COUNT(*) FROM features_company WHERE (LOWER(features_company.business_model) = 'b2b') AND (USER_id = 123) ORDER BY 1 DESC;
```

### Example of Schema

!!! note "columns and SqlName are mandatory"

```json
{
  "sourceName": "Acquisitions1", # this is not mandatory 
  "columns": [
    {
      "SqlName": "company",
      "defaultOnSelect": true,
      "distinctValues": [
        "whatsapp"
      ],
      "name": "startup, company", # the name in natural language 
      "type": "text", # type in DB can be one of [text time ,boll , number,...
      "nullable": "", #I need to check if we are using this 
      "id": "13313121", # not mandatory 
      "defaultEntity": false 
    }, ... ]

  "id": "55530345", # not mandatory 
  "SqlName": "acquisitions", # Table sql name 
  "tableName": "acquistions", # Not mandatory - NLP name of the data source 
  "jaqlSupport": "no", # not mandatory default  to no 
  "timezone": "", #not mandatory default  to empty 
  "executeSql": "yes", # not mandatory default  to no  
  "connectionString": "" # not mandatory default  to no  
}
```


#### Output : JSON 

Name  | Type | Description
------------- | -------------- | -------------
`origQustion` | `string` | The question you asked.
`sql` | `string` | The translation.
`results` | `list` | List of resuld ecry raw id a dictionary like with column name , value.
`JAQL` | `dict` | JAQL representation of the sql query.
`Error` | `string` | IF Error Acord its reason.

#### Request Headers
Name | value 
------------ | -------------
`Content-type` | `application/json`
`Accept` | `text/plain`

### Example
```json
{ 
  question : "How many whatsapp",
  maxResult : 5,
  schema :  {
    "sourceName": "Acquisitions4",
    "columns": [{
        "SqlName": "company",
        "defaultOnSelect": true,
        "distinctValues": [
            "whatsapp"
        ],
        "name": "startup, company",
        "type": "text",
        "nullable": "",
        "id": "13313121",
        "defaultEntity": false
    }],
    "id": "55530345",
    "SqlName": "acquisitions",
    "tableName": "acquistions",
    "jaqlSupport": "no",
    "timezone": "",
    "executeSql": "yes",
    "connectionString": ""
}
```

### CURL Request

```
curl 'https://ask.nibi.ai/api/ask/v2/schema_nibitalkres' -H 'Accept: text/plain' -H 'Referer: https://ask.nibi.ai/' -H 'Origin: https://ask.nibi.ai' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36' -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NDExMjE1MDAsIm5iZiI6MTU0MTEyMTUwMCwianRpIjoiNzdhMTlkOTUtYTIzOC00NDIxLWJiYTYtNzVhOGMzMzkxYzJiIiwiZXhwIjoxNTQxMTIzNTAwLCJpZGVudGl0eSI6eyJpZCI6NSwib3JncyI6W10sInNvdXJjZV9yb2xlIjpbeyJkcyI6IlNpc2Vuc2UiLCJyb2xlIjoiZGF0YVNvdXJjZVZpZXdlciJ9XSwiYXBwX3V1aWQiOjc3fSwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.29ml072DafV4kuVyJdTvizkI3LckVyG97GpFqOzMq7k' -H 'Content-Type: application/json' --data-binary '{"question":"How many whatsapp","maxResults":5,"schema":{"sourceName":"Acquisitions4","columns":[{"SqlName":"company","defaultOnSelect":true,"distinctValues":["whatsapp"],"name":"startup, company","type":"text","nullable":"","id":"13313121","defaultEntity":false}],"id":"55530345","SqlName":"acquisitions","tableName":"acquistions","jaqlSupport":"no","timezone":"","executeSql":"yes","connectionString":""}}' --compressed
```

### Response
```json
{
	"origQustion": "How many whatsapp",
	"sql": "\nSELECT acquisitions.company AS \"startup\"\nFROM acquisitions\nWHERE LOWER(acquisitions.company) = 'whatsapp';\n",
	"results": [{
		"startup": "WhatsApp"
	}],
	"Error": ""
}
```
