
## GET 
URL : `/api/history/v1/questions`
Method : `GET`

Login required   | Allowed Users     | Methods   
-------------| -------------- | -------------
Yes | All  | GET

!!! note "Authorization"
User must have at least view permission to data source 
**Parameters in query string**

### Parameters
Name | Describe | Note   
-------------| -------------- | -------------
time_from | Data range (from to)  | Iso format ('2008-08-09T18:39:22Z')
time_to | Data range (from to) | Iso format ('2008-08-09T18:39:22Z')
org_id  | org_id | Default  0
user | User who asked the questions: me-current user , or all | Default : me
data_source | Equal | Use current datasource
Text_in_ question | Optional | If not specify, don’t filter on it
error | Optional | Default - Error is NULL ( meaning bring all questions that are valid
page_size |     | Default 100
page_number |    | 

## Example

For me: `/api/history/v1/questions?Datasource=google&page_number=1`
For all users of the datasource: `/api/history/v1/questions?Datasource=google&page_number=1&user=all`

Return Json with results as list of dictionary

### Response

!!! success

```json
{
  'res':res_list,
  'page_size': pagination.page_size,
  'page_number': pagination.page_number,
  'num_pages':  pagination.num_pages,
  'total_results': pagination.total_results
 }
```

!!! error
``` 
{ 
  "msg":"ERROR in user terms"
  } 
```

