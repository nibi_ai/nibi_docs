## /user/register
------------------------------------------------------------

Login Required  | Allowed Users  | Methods 
-------------| -------------- | -------------
Yes |  All  | POST

### Parameters 
Name  | Describe  | Note 
-------------| -------------- | -------------
`email` |  The User email  | Mandatory
`username` | The user user name | Not mandatory
`password` | Password for the user | Mandatory
`retype_password` | The same as password | Mandatory

Return if an invite was sent  and message