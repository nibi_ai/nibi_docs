# Nibi Search + React

**Version: 1.0**

**Last updated: 19 November 2018**

React Component is a npm module that can be use as a dependency in react project. 

## React Component Props

Property  | Optional/Required | Description
------------ | ------------- | -------------
`config` | Required |  [Configuration properties](index.md) 
`options` | `optional` | [ Options (Settings)](index.md) 
`onSearch` | `optional` | [Events (callbacks)](index.md) 
`onBeforeSearch` | `optional` | [Events (callbacks)](index.md) 


#### 1. Get Access Token

[User Access Token](../accessToken/user_access_token.md)

[Datasource Access Token](../accessToken/datasource_access_token.md)

#### 2. Install NIBI SEARCH

`npm install nibi.ai.nlq@latest --save`

#### 3. Import dependency 
```
import NLQSearch from 'nibi.ai.nlq'
import 'nibi.ai.nlq/build/styles.css';
```

[Advance configuration and settings](../widget/index.md) 

### Example
```jsx
import React, { Component } from 'react'

import NLQSearch from 'nibi.ai.nlq'
import 'nibi.ai.nlq/build/styles.css';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nibiOptions : {
        placement : 'TOP'
      },
      nibiConfig : {
        datasource: 'DATASOURCE', //Required
        apiKey: 'API_KEY' //Required
      }
    }
  }
  onSearch = (message) => {
    console.log('onSearch message', message);
  }

  beforeAskQuestion = (npl) => {
    console.log('beforeAskQuestion npl', npl);
  }

  render() {
    return (
      <div>
        <NLQSearch config={this.state.nibiConfig} options={this.state.nibiOptions} onSearch={this.onSearch} onBeforeSearch={this.beforeAskQuestion} />
      </div>
    )
  }
}

```

