
## /api/admin/v1/api_tokens
--------------------------------------------------------
Method : `GET`

Get list of tokens or create a new token to use with API. 
THe API token allow you to get a token so you can use without logging in to the web interface. 

Login required   | Allowed Users     | Methods   
-------------| -------------- | -------------
Yes | All  | POST ,GET

### Parameters
None

### Response
Return Json with list of tokens parameters 
```json
 [ 
   {
     "token_uuid": "46badc21-443e-4d38-8123-e4a9cd520eba", 
      "expires": "2019-09-26 16:45:07.615492"
   },
   {
     "token_uuid": "8091bcb2-35de-448b-9905-357f59a5df03", 
      "expires": "2019-09-26 16:53:17.989137"
  }
]
```
