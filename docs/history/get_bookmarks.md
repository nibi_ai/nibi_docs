

###Method GET
--------------------------------------------------
URL : `/api/history/v1/bookmarks`

Method : `GET`

Login required   | Allowed Users     | Methods   
-------------| -------------- | -------------
Yes | All  | GET

!!! note "Authorization User must have at least view permission to data source"

### Parameters : Query String
Name  | Describe     | Note  
-------------| -------------- | -------------
`data_source` | Equal | Use current datasource
`question` | Optional | If not specify, don’t filter on it
`page_size` |     | Default 100
`page_number`|    | 

### Request

For me: ```/api/history/v1/questions?Datasource=google&page_number=1```

For all users of the datasource:

Return Json with results as list of dictionary

### Response

!!! success
```json
{
       'res':res_list,
       'page_size': pagination.page_size,
       'page_number': pagination.page_number,
       'num_pages':  pagination.num_pages,
       'total_results': pagination.total_results
 }
```

!!! error

```json
{ 
  "msg":"ERROR in user terms"
}
```