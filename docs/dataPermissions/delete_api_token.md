
## /api/admin/v1/api_tokens/{tok_uuid}/delete
------------------------------------------------------
Method : `DELETE`

Delete the token with the uuID

Login required   | Allowed Users     | Methods   
-------------| -------------- | -------------
Yes | All - operations for users tokens only  | DELETE

### Parameters
Name   | Describe  | Note   
-------------| -------------- | -------------
tok_uuid | The token id you want to to delete  | Mandatory @ URL 

### Response
Return True if success
