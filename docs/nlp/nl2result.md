The AIP will implement the functionality of getting an SQL form a neutral language sentence. 

URL : `nibitalkres/{table_name}`

Method : `POST`

#### Parameters
Name  | Type | Description
------------- | -------------- | -------------
`Question` | `string` | The question you  want translated to SQL.
`maxResults` | `string` /int | The max number of results you want to get.
`filter` |  | Fitler - this is used for adding a filter to the where clause that Niibi is generating. You enter the exact format it will be used in the SQL. The filter will be in relation AND to the conditions in the natural language sentence . 

#### How to use filter
For instance if you ask the question `How many companies are B2B` With the filter `USER_id = 123` you will get the following query:

```sql
SELECT COUNT(*) FROM features_company WHERE (LOWER(features_company.business_model) = 'b2b') AND (USER_id = 123) ORDER BY 1 DESC;
```


#### Output : JSON 

Name  | Type | Description
------------- | -------------- | -------------
`origQustion` | `string` | The question you asked.
`sql` | `string` | The translation.
`results` | `list` | List of resuld ecry raw id a dictionary like with column name , value.
`JAQL` | `dict` | JAQL representation of the sql query.
`Error` | `string` | IF Error Acord its reason.

#### Example
```json
{ 
  "question":"Show me b2b companies founded last year",
  "maxResults":"10"
}
```

#### CURL Request
```
curl -i -H "Content-type: application/json" -X POST -d "{\"question\":\"Show me b2b companies founded last year\",\"maxResults\":\"10\" }"  http://127.0.0.1:8080/nibitalkres/LPLP
```
#### Response
```json
{
	"origQustion": "Show me b2b companies founded last year",
	"sql": "\nSELECT *\nFROM features_company\nWHERE LOWER(features_company.business_model) = 'b2b' AND features_company.founded_at >= '2017-01-01T00:00:00.000-08:00';\n",
	"results": [
    	{
        	"id": "4",
        	"company_id": "7810",
        	"name": "NevaBio",
        	"website": "None",
        	"sector_id": "16",
        	"sector_name": "Other",
        	"funding_stage_id": "1",
        	"funding_stage_name": "Bootstrapped",
        	"founded_at": "2017-10-01",
        	"proba_prediction_false": "None",
        	"proba_prediction_true": "None",
        	"label": "None",
        	"full_profile": "https://app.nibi.ai/pioneer/companies/7810",
        	"business_model": "B2B"
    	}
    ]
}
```