## POST(org)
--------------------------------------------------------
URL : `/api/admin/v1/orgs`

Method : `GET`

Will create a new org and will make you the org Admin if you already belong to an org you cannot create an org

Login required   |   Allowed Users  | Method
------------ | ------------- | -------------
Yes | All - No effect if you already belong to an org | POST

### Parameters

Name  | Describe  | Note
------------ | ------------- | -------------
name | The name the org will be given | Mandatory


### Response : JSON
Return the created org name and id 
```json
 {
   'org_id':u_org.id,
   'org_name':u_org.name
}
```

!!! note "/api/admin/v1/org_users"
This Endpoint will allow you  to create users for your org and get a list of user ids for your org 
This Endpoint is accessed by jwt token 

Login required   |   Allowed Users  | Method
------------ | ------------- | -------------
Yes | OrgAdmin| POST
