# Overview

This endpoint will allow you create an org when you create an org you get the org Admin role and you can create users that belong to that org 
Endpoint is accessed with token 

## GET(org)
--------------------------------------------------------
URL : `/api/admin/v1/orgs`

Method : `GET`

Login required   | Allowed Users   | Methods 
------------- | -------------- | -------------
Yes | All | GET


### Parameters
None

### Response : JSON
Return Json with list of orgs.
```json
 {
   "org":[
     {"name":"my org","id":1},
     {"name":"my org1","id":2}]
    ]
}
```