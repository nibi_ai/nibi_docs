
## DELETE
URL : `/api/history/v1/bookmark/<bookmark_id>`

Method : `DELETE`

This endpoint will allow you to insert question  to the loger get questions and query questions

Login required   | Allowed Users     | Methods   
-------------| -------------- | -------------
Yes | All  | post

### Parameters : Query String
Name  | Describe     | Note  
-------------| -------------- | -------------
bookmark_id  | The bookmark id you want to delete  | 

### Response

!!! success
```json
{
  'id':bk.id
 }
```