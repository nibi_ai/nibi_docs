## /api/admin/data_source/{ds_name}/create 
------------------------------------------------------------
Create a Data source for me as a dataSourceOwner

Login required   | Allowed Users   | Methods 
-------------| -------------- | -------------
Yes | All  | GET

 Return success if Data source does not exist before 
 
### Response
```json
{
  "action": true,
  "msg": "Data source Data_test Was created"
}
```