# Nibi overview

**Version: 1.0**

**Last updated: 21 November 2018**

Nibi is fostering a data-driven culture within organizations by enabling SaaS and BI vendors to develop a natural language interface to their data. Instead of wasting endless efforts to make your dashboards more useful or answering requests from your clients to provide them with the data they need, embed Nibi’s natural language search engine and enjoy higher engagement and user satisfaction. 

To list several of our key features: 

*  **Embeddable and API based**

  Our product can be embedded as a react component which is fully customizable or you can develop your own search app using our API

*  **Connected to any database**

We are connected to most of the common databases out there [see a list here](https://nibi.ai/partners/)


*  **Secured**

Our engine is fully secured as we use advanced authentication methods. Also, note that there no need to migrate your data to 
our servers

*  **On the fly set up**

We read the schema of your data on the fly and from that point onwards, it’s accessible via natural language. No need to train or run ad-hoc set up for different tables

*  **Get to know your users**

You get access to the history of your users' questions to better understand their data related needs to improve your product

*  **Auto-complete**

Nibi empowers your users to explore untapped areas in your data universe by using our auto-complete and suggestion engine

*  **Follow up questions**
Amplify your users' analytical skills by leveraging our AI based suggested follow up questions engine to help them navigate your data and reach an insight by asking the right questions

For more information, please check out nibi.ai or reach out to `yehuda@nibi.ai`.
