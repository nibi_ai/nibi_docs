# Overview

## Test Answer
----------------------------------------------------------

Curl Request example

#### Request : Json

```command
curl -i -H "Content-type: application/json" -X POST -d "{\"question\":\"Show me b2b companies\" }"  http://127.0.0.1:8080/nibitalk/lplp?
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 179
Server: Werkzeug/0.14.1 Python/3.6.3
Date: Sun, 10 Jun 2018 13:43:39 GMT

```

#### Response : Json

```json
{
  "origQustion": "Show me b2b companies",
  "sql": "\nSELECT features_company.company\nFROM features_company\nWHERE features_company.sector = \"b2b\";\n",
  "Error": ""
}
```