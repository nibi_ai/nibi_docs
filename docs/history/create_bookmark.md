
## POST
--------------------------------------------------
URL : `/api/history/v1/bookmarks`

Method : `POST`

Login required   | Allowed Users     | Methods   
-------------| -------------- | -------------
Yes | All  | post

### Parameters : JSON
Name  | Describe     | Note  
-------------| -------------- | -------------
`time` | The time the question was asked | Not a parameter field in the table
`org_id` | The Org ID of the user  |
`data_source` | The Data source the question was asked about | Mandatory
`question` | The question in natural language | Mandatory

### Response : JSON
!!! success

```json 
{
  "action":True,
   "msg":"OK",
   "ID":"the question id" }
```

The question is is a unique identifier for the question so we can later set it as bookmark and restore it by ID or shae by id

!!! error

```json
{
  "action":False,
   "msg":"ERROR in user terms"
   }
```