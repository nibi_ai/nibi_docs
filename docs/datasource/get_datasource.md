# Overview

Download of configuration file for Data Source.  <data_source> is the file you want to CRUD 
KEEP in mind you need to upload both the json and the sql for the data source to work.

## Get Datasource
----------------------------------------------------------
URL : `/v1/nibiconf/data_source/<data_source>`

Method : `GET`


#### Parameters
Name  | Type | Description
------------- | -------------- | -------------
`data_source` | `string` | A datasource name that will be passed as path variable

#### Output : JSON 
The file in a json format 
```json
{
  "id": "13837135",
  "SqlName": "SqlName",
  "columns": [
    {
      "name": "label" ,
      "type": "text",
      "SqlName": "sql_name_label",
      "distinctValues": [],
      "Entity": "location"
    }
  ],
  "timezone": "UTC-12:00",
  "jaqlSupport": "yes",
  "executeSql": "yes",
  "name": "name"
}
```