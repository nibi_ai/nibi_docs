
## /data_permission/<ds_name>/add_user
--------------------------------------------------------
Login required   | Allowed Users   | Methods 
-------------| -------------- | -------------
Yes | dataSourceOwner for data source Or Admin has acces for all data sources | GET

### Parameters

Name | Describe  | Note
-------------| -------------- | -------------
email | The User email  | Mandatory
role | The user user role | Not mandatory

### Request : JSON

```json
{
"email":"admin@nibiexample.com",
"role":"dataSourceEditor"
}
```

### Response : JSON
```json
{
  "action":True,
   "msg":"OK"
}
```
