
### Generate API Key

Login to your account in admin and click on Developer settings. Please refer to below screen
![Dev Setting](../images/dev-seeting.png "Dev Setting")
 
Once you click on `add new`, you will be asked for API key name
![create api key](../images/create-api-key.png "create api key")


Click on `Create`
![create api key](../images/create_api.png "create api key")

 
We highly recommend you to keep API key secure and safe. We do not regenerate this API key. 

