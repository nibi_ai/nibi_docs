## DELETE(user)

URL :  `/api/admin/v1/org_users/<user_id>`

Method : 'DELETE'

Login required   |   Allowed Users  | Method
------------ | ------------- | -------------
Yes | OrgAdmin | DELETE

Will delete the user 

### Parameters: Query String

Name  | Describe  | Note
------------ | ------------- | -------------
userId | user id  | Mandatory

### Response

!!! success "status code - 200"

