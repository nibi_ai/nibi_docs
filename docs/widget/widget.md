# Nibi Search overview

**Version: 1.0**

**Last updated: 20 November 2018**


Nibi widget is a embeddable component that works independently in your webpage. Nibi widget is a ReactJs component that works extremely fast in comparison to  other(AngularJs, emberjs) because of ReactJs virtual DOM and Javascript driven templates.

In order to use the widget you need to get a access token that expires in 24 hrs. NIbi widget is going to use Access token only that is temporary token that means nobody can use this token more than 24hrs. To get the serve side access token you need to have API_KEY that is safe and not visible to user on client. 

The advantage of using widget that you can customise it completely by importing your css. Nibi widget has a prefix css class `.nibi-widget` that can be used to extend styling/theme of widget.

An example of theme customization
```
.nibi-widget .message-container{
  background-color : ‘#ccc’
}
```
In the same way, color, font-size can be changed.


## Setup widget

#### 1. Get Access Token

[User access Token](../accessToken/user_access_token.md)

[Datasource Access Tpken](../accessToken/datasource_access_token.md)

#### 2. Import NIBI Widget Liabrary
```
<link rel="stylesheet" href="https://ask.nibi.ai/widget/nibi-ai-widget.v1.css" />
<script type="text/javascript" src="https://ask.nibi.ai/widget/nibi-ai-widget.v1.js"></script>
``` 


#### 3. Create DOM element with unique ID

To enable the basic widget, add the following to your page before closing <body> tag:

```
<div id="widget"  ></div>
```


#### 4. Use below script in the footer section just before </body> tag
```javascript
  <script>
    NIBI_AI.config({
      API_KEY: '', // This is access token.
      DATASOURCE_NAME: '', // DATASOURCE_NAME - required,
      DATASOURCE_SCHEMA : {} // datasource configuration - json format
    });

    //create an instance of widget
    var widget = NIBI_AI.widget.autoComplete.new({
      selector: "#widget",
      placement : 'TOP|BOTTOM'
    });
    //render widget
    widget.render();
  </script>
```

[Advance configuration and settings](../widget/index.md) 

## Advance example

lets create an instance of autocomplete widget. A widget can be destroy by calling `widgetInstance.destroy()`. We recommed to destroy widget if you switch configurations.

```javascript

const widget = new NIBI_AI.autocomplete({
 selector: "#INPUT_FIELD_ID",
 placement : ‘TOP|BOTTOM’, 
 events: {
   onInit: () => {
     // event is triggered when widget instance is created
   },
    :   (npl: string, suggestions: Array) => {
     //event is triggered when data is typed and returns whole entered NPL string
     // element of array {"full":"How many companies that","suggestion":"that","meta":{}}  
   },
   onSearch: (result: any) => {
     //event is triggered when question is asked and returns result from API
   }
 }
});

//You can destroy the instance by calling destory method.
widget.destroy();
```