## /api/admin/getjwt
------------------------------------------------------------
Will be replaced by

``` /api/admin/v1/get_user_token ```

Will retune  a token to use with the NIBI API. Tokens are to be used in request headers

!!! note "Authorization: Bearer xyzMyTokenString…./r/n"

Login Required  | Allowed Users  | Methods 
-------------| -------------- | -------------
Yes |  All  | POST.GET

### Response
```json
{
'access_token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Mzc5NzU4NTksIm5iZiI6MTUzNzk3NTg1OSwianRpIjoiMThjZDVjZjQtYzI5Ni00ODZkLTg1MDctMjJiMDUwODNiMzc2IiwiZXhwIjoxNTM4MDYyMjU5LCJpZGVudGl0eSI6eyJpZCI6MX0sImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.VKebjV3NrFpFkMJqh5B9TsCEp40cOAhaaB0ChvKMGEE', 
'expiration': 86400.0
}
```