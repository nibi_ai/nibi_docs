# Overview
Delete configuration file

## Delete DELETE
----------------------------------------------------------
URL : `/v1/nibiconf/data_source/<data_source>`

Method : `DELETE`

### Request :JSON

This command will also sync all configuration with the new files.

Name  | Type | Description
------------- | -------------- | -------------
`data_source` | `str` | The name of the data source.


### Response : JSON

Name  | Type | Description
------------- | -------------- | -------------
JsonFile | str | The Json file that was deleted.
SqlFile | str | The SQL file that was deleted .
Error | str | If error accord .
