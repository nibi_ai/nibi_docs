## GET(user)
URL :  `/api/admin/v1/org_users`

Method : 'GET'


### Parameters
None

### Response : JSON
Return list of user ids
```json
 {
  'user_ids': ['1', '3', '4']
}
```