```php
<?php

class NibiAuth
{
   protected $API_KEY = 'YOUR API_TOKEN';
   protected $API_URL = 'https://ask.nibi.ai/api/admin/v1/api_token/get_user_token';


   public function getAccessToken()
   {
       $ch = curl_init($this->API_URL);
       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
       curl_setopt($ch, CURLOPT_POSTFIELDS, 0);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER,
           array(
               'Authorization: Bearer ' . $this->API_KEY,
           ));
       curl_setopt($ch, CURLOPT_TIMEOUT, 5);
       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
       $response = curl_exec($ch);
       curl_close($ch);
       return $this->responseHandler($response);
   }

   private function responseHandler($response)
   {
       $result = json_decode($response, true);
       if (!is_object($result) && !is_array($result)) {
           return '';
       }
       return isset($result['access_token']) ? $result['access_token'] : '';
   }
}

$auth = new NibiAuth();
$token = $auth->getAccessToken();
?>
<!DOCTYPE html>
<html lang="en" class="default-style">
<head>
   <title>Source Configuration</title>
   <script>
       var api_token = '<?=$token?>';
   </script>
   <script type="text/javascript" src="https://ask.nibi.ai/widget/nibi-ai-widget.v1.js"></script>
   <link rel="stylesheet" href="https://ask.nibi.ai/widget/nibi-ai-widget.v1.css"/>

</head>
<body>

<div id="widget"></div>
<div id="widget-output"></div>

<script>
   /**
    * configure NIBI_AI widget
    * */
   NIBI_AI.config({
       API_KEY: api_token, // API_KEY is same as access token
       DATASOURCE_NAME: ‘’// DATASOURCE_NAME
   })
   ;
   /**
    * Create Instance widget
    * */
   var widget = NIBI_AI.widget.autoComplete.new({
       selector: "#widget",
       placement: 'TOP',
       events: {
            onSearch: (result) => {
               document.getElementById('widget-output').value = JSON.stringify(result);
           }
       }
   });
   widget.render();

</script>
</body>
</html>
```