# Overview

## Get user access token
---------------------------------------------------------------

URL : `/api/admin/v1/api_token/get_access_token`

Method : `POST `

Get a token you can use in the nibi API as the user that created the token 

Login required   | Allowed Users     | Methods   
-------------| -------------- | -------------
Yes | All - operations for users tokens only  | POST

!!! note "This token expires in 24 hours after its creation"

### Parameters
None

### Response : Json

!!! success "Status code 200"

```json
{
'access_token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Mzc5NzU4NTksIm5iZiI6MTUzNzk3NTg1OSwianRpIjoiMThjZDVjZjQtYzI5Ni00ODZkLTg1MDctMjJiMDUwODNiMzc2IiwiZXhwIjoxNTM4MDYyMjU5LCJpZGVudGl0eSI6eyJpZCI6MX0sImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.VKebjV3NrFp', 
'expiration': 86400.0
}
```



## Example

To get the access token you must have an valid [API Key](../user/api_key_example.md). Due to security reasons, we ask developer to get access token on server side. Once you get access token that is valid for next 24hrs. Refer to the screen in the admin. 
Server side Example of 


We have written an example in PHP,  You can use any server side language [python|node|java etc]


```php
<?php 
class NibiAuth {
protected $API_KEY = 'YOUR API TOKEN; // API Key that you generate from Admin.

protected $API_URL = 'https://ask.nibi.ai/api/admin/v1/api_token/get_user_token';
  public function getAccessToken() {
       $ch = curl_init($this->API_URL);
       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
       curl_setopt($ch, CURLOPT_POSTFIELDS, 0);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER,
           array(
               'Authorization: Bearer ' . $this->API_KEY,
           ));
       curl_setopt($ch, CURLOPT_TIMEOUT, 5);
       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
       $response = curl_exec($ch);
       curl_close($ch);
       return $this->responseHandler($response);
   }

   private function responseHandler($response)
   {
       $result = json_decode($response, true);
       if (!is_object($result) && !is_array($result)) {
           return '';
       }
       return isset($result['access_token']) ? $result['access_token'] : '';
   }


}
$auth =  new NibiAuth();
$token = $auth->getAccessToken();
?>
```

