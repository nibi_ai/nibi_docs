# Overview
Download of configuration file for Data Source.  <data_source> is the file you want to CRUD 
KEEP in mind you need to upload both the json and the sql for the data source to work.


## Update Datasource
----------------------------------------------------------
URL : `/v1/nibiconf/data_source/<data_source>`

Method : `PUT`

### Request :JSON

This command will also sync all configuration with the new files 
The json file (described below that describe the data source).

Name  | Type | Description
-------------| -------------- | -------------
`SqlName` | `str` | SqlName
`columns` | `Array` | columns
`connectionString` | `str` | connectionString
`executeSql` | `str` |executeSql
`id` | `string` | id
`jaqlSupport` | `str` | jaqlSupport
`name` | `str` | name.
`timezone` | `str` | timezone.

### Response : JSON

Name  | Type | Description
------------- | -------------- | -------------
Success | Boolean | True is success.
Error | str |IF Error Acord its reason.